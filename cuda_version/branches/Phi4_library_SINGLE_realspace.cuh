/*
 * cufftw_wrap-phi4.hpp
 *
 *  Created on: Jul 11, 2011
 *      Author: eze, ale
 */

#ifndef PHI4_LIBRARY_CUH
#define PHI4_LIBRARY_CUH

// RNG: PHILOX
#include <Random123/philox.h>
#include <Random123/u01.h>
typedef r123::Philox2x64 RNG2;
typedef r123::Philox4x64 RNG4;

//#include "../common/common.c"
#include <stdint.h> /* uint8_t */

#include "thrust_predicates.cuh"

using namespace std;

texture<FLOAT,1,cudaReadModeElementType> qmoduleTex;
texture<FLOAT,1,cudaReadModeElementType> disorderTex;


/*--------RNG ROUTINES--------*/
__device__ FLOAT rand_MWC_normal(unsigned long long* x, unsigned int* a)
{
  // Use Box-Muller algorithm
  // Get normal (Gaussian) random sample with mean 0 and standard deviation 1
  FLOAT u1 = rand_MWC_oc(x, a);
  FLOAT u2 = rand_MWC_oc(x, a);
  FLOAT r = sqrt( -2.0*log(u1) );
  FLOAT theta = 2.0*M_PI*u2;
  return r*sin(theta);
}

__device__
FLOAT DeviceBoxMuller(const FLOAT u1, const FLOAT u2)
{
#ifdef DOUBLE_PRECISION
	FLOAT r = sqrt( -2.0*log(u1) );
	FLOAT theta = 2.0*M_PI*u2;
	return r*sin(theta);
#else
	FLOAT r = sqrtf( -2.0*logf(u1) );
	FLOAT theta = 2.0*M_PI*u2;
	return r*sinf(theta);
#endif
}

/*
__device__
void PhiloxRandomQuartet(const unsigned int index, const unsigned int time, FLOAT *r1, FLOAT *r2, FLOAT *r3, FLOAT *r4)
{
	RNG4 rng;
	RNG4::ctr_type c_pair={{}};
	RNG4::key_type k_pair={{}};
	RNG4::ctr_type r_quartet;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= 2*time+1;
		c_pair[1]= SEED; // eventually do: SEED + sample;
		// random number generation
		r_quartet = rng(c_pair, k_pair);

		*r1= u01_closed_closed_64_53(r_quartet[0]);
		*r2= u01_closed_closed_64_53(r_quartet[1]);
		*r3= u01_closed_closed_64_53(r_quartet[2]);
		*r4= u01_closed_closed_64_53(r_quartet[3]);

}

__device__
void PhiloxRandomPair(const unsigned int index, const unsigned int time, FLOAT *r1, FLOAT *r2)
{
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= 2*time;
		c_pair[1]= SEED; // eventually do: SEED + sample;
		// random number generation
		r_pair = rng(c_pair, k_pair);

		*r1= u01_closed_closed_64_53(r_pair[0]);
		*r2= u01_closed_closed_64_53(r_pair[1]);
}
*/

//---------- DEVICE FUNCTIONS -----------//

__global__ void CUDAkernel_InitRandom(FLOAT *a, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
			a[index] = 2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}
	d_x[tid] = rng_state; // store RNG state into global again
}

__global__ void CUDAkernel_SetQxQy(FLOAT *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		// Using cos(qx) cos(qy) boundary conditions are automatically determined by the function periodicity otherwhise modulo "%" operations are required.
		FLOAT qx=2.f*M_PI/(FLOAT)LX*idx;
		FLOAT qy=2.f*M_PI/(FLOAT)LY*idy;
		int index = idx*ny + idy;
		qqmodule[index]=-2.f*(cosf(qx)+cosf(qy)-2.f);

		//int nxs2=(nx/2); int nys2=(ny/2);
		//FLOAT qx=2*M_PI/(FLOAT)LX*((idx+nxs2)-nx);
		//FLOAT qy=2*M_PI/(FLOAT)LY*((idy+nys2)-ny);
		//int index = ((idx+nxs2)%nx)*ny + ((idy+nys2)%ny); 
		//qqmodule[index]=qx*qx+qy*qy;
	}
}

__global__ void CUDAkernel_SetDisorder(FLOAT *d_disorder, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	unsigned int rng_const = d_a[tid];

	unsigned int i;
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
		#ifndef UNIFORM_DISORDER
			d_disorder[index] = C_EPSILON*(rand_MWC_normal(&rng_state, &rng_const));
		#else
			d_disorder[index] = C_EPSILON*3.46410162*(rand_MWC_oc(&rng_state, &rng_const)-0.5);
		#endif
//			d_disorder[index] = C_EPSILON*2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}	
	d_x[tid] = rng_state; // store RNG state into global again
}

__global__ void CUDAkernel_Real2RealScaled(FLOAT *a, int nx, int ny, FLOAT scale){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		a[index]= scale*a[index];
	}	
}

__global__ void complex2complex_scaled(cufftComplex *a, int nx, int ny, FLOAT scale){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		a[index].x= scale*a[index].x;
		a[index].y= scale*a[index].y;
	}	
}


//__global__ void CUDAkernel_CalculateRealForce(const FLOAT *e, const cufftComplex *a, cufftComplex *b, int nx, int ny, FLOAT uniform_field){
__global__ void CUDAkernel_CalculateRealForce(const FLOAT *e, const FLOAT *a, FLOAT *b, int nx, int ny, \
						const FLOAT uniform_field, const FLOAT offset, const FLOAT disordershiftx){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;

	#ifndef ZERO_TEMPERATURE
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];
	#endif

/*		FLOAT r1, r2, r3, r4;
		//RNG CALL
		//PhiloxRandomPair(idx, rng_counter, &r1, &r2);
		PhiloxRandomQuartet(idx, rng_counter, &r1, &r2, &r3, &r4);
*/
	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
// 			const FLOAT epsilon = e[index];
//			const FLOAT epsilon = tex1Dfetch(disorderTex, index);

			// indices del desorden shifteado involucrados en la interpolacion de mas abajo
			int j1=int(j+disordershiftx)%ny;
			int j2=int(j+1+disordershiftx)%ny;

/*			FLOAT r1, r2;
			PhiloxRandomPair(index, j1, &r1, &r2);
			const FLOAT epsilon1 = C_EPSILON*DeviceBoxMuller(r1,r2); //2*(r1-0.5); 
			PhiloxRandomPair(index, j2, &r1, &r2);
			const FLOAT epsilon2 = C_EPSILON*DeviceBoxMuller(r1,r2); //2*(r1-0.5); 
*/
			const FLOAT epsilon1 = tex1Dfetch(disorderTex, i*ny + j1);
			const FLOAT epsilon2 = tex1Dfetch(disorderTex, i*ny + j2);

			FLOAT w = (disordershiftx-int(disordershiftx)); // w < 1
			FLOAT epsilon = epsilon1*(1.0-w) + epsilon2*w; // interpolated disorder

//			const FLOAT laplacian = a[((i-1+ny)%ny)*ny + j]+a[((i+1)%ny)*ny + j]+a[i*ny + (j-1+nx)%nx]+a[i*ny + (j+1)%nx]-4*a[index];
			//laplacian with anti-periodic boundary conditions
			const FLOAT laplacian = a[((i==0)*(nx-1)+(i!=0)*(i-1))*ny + j] + \
						a[((i!=nx-1)*(i+1))*ny + j] + \
						(1-2*(j==0))*a[i*ny + ((j==0)*(ny-1)+(j!=0)*(j-1))] + \
						(1-2*(j==ny-1))*a[i*ny + ((j!=ny-1)*(j+1))] - \
						4*a[index];

			FLOAT drive = uniform_field*(j-ny*offset);

			#ifdef ZERO_TEMPERATURE
			b[index] = C_BETA*laplacian + (C_ALPHA + epsilon)*a[index] - a[index]*a[index]*a[index] + drive;
			#else
			b[index] = C_BETA*laplacian + (C_ALPHA + epsilon)*a[index] - a[index]*a[index]*a[index] + drive\
			+ XTEMP*(rand_MWC_co(&rng_state, &rng_const)-0.5);
			#endif
		}
	}
	#ifndef ZERO_TEMPERATURE
	d_x[tid] = rng_state; // store RNG state into global again
	#endif
}

__global__ void euler_step_rs(FLOAT *a, const FLOAT *b, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		a[index] = a[index] + DT*b[index];
	}	
}

/*
__global__ void euler_semiimplicit_step(cufftComplex *a, const cufftComplex *b, const FLOAT *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		int index = idx*ny + idy;
		//const FLOAT fac=-(qqmodule[index]);
		const FLOAT fac=-(tex1Dfetch(qmoduleTex, index));
		const FLOAT dip = -(9.05f-2.f*M_PI*sqrtf(-1.f*fac));
		a[index].x = (a[index].x*(1.f+C_GAMMA*dip*DT) + DT*b[index].x)/(1.f-fac*C_BETA*DT);
		a[index].y = (a[index].y*(1.f+C_GAMMA*dip*DT) + DT*b[index].y)/(1.f-fac*C_BETA*DT);
	}
}
*/

/* RGB formulae.
 * http://gnuplot-tricks.blogspot.com/2009/06/comment-on-phonged-surfaces-in-gnuplot.html
 * TODO: cannot find what to do with over/under flows, currently saturate in [0,255]
 *
 * there are 37 available rgb color mapping formulae:
 * 0: 0               1: 0.5             2: 1
 * 3: x               4: x^2             5: x^3
 * 6: x^4             7: sqrt(x)         8: sqrt(sqrt(x))
 * 9: sin(90x)        10: cos(90x)       11: |x-0.5|
 * 12: (2x-1)^2       13: sin(180x)      14: |cos(180x)|
 * 15: sin(360x)      16: cos(360x)      17: |sin(360x)|
 * 18: |cos(360x)|    19: |sin(720x)|    20: |cos(720x)|
 * 21: 3x             22: 3x-1           23: 3x-2
 * 24: |3x-1|         25: |3x-2|         26: (3x-1)/2
 * 27: (3x-2)/2       28: |(3x-1)/2|     29: |(3x-2)/2|
 * 30: x/0.32-0.78125 31: 2*x-0.84       32: 4x;1;-2x+1.84;x/0.08-11.5
 * 33: |2*x - 0.5|    34: 2*x            35: 2*x - 0.5
 * 36: 2*x - 1
 * 
 * Some nice schemes in RGB color space
 * 7,5,15   ... traditional pm3d (black-blue-red-yellow)
 * 3,11,6   ... green-red-violet
 * 23,28,3  ... ocean (green-blue-white); try also all other permutations
 * 21,22,23 ... hot (black-red-yellow-white)
 * 30,31,32 ... color printable on gray (black-blue-violet-yellow-white)
 * 33,13,10 ... rainbow (blue-green-yellow-red)
 * 34,35,36 ... AFM hot (black-red-yellow-white)
 */

#define RGBf05 (x*x*x)
#define RGBf07 (sqrt(x))
#define RGBf15 (sin(2*M_PI*x))
#define RGBf21 (3*x)
#define RGBf22 (3*x-1)
#define RGBf23 (3*x-2)
#define RGBf30 (x/0.32-0.78125)
#define RGBf31 (2*x-0.84)
#define RGBf32 (x/0.08-11.5)
#define RGBf34 (2*x)
#define RGBf35 (2*x - 0.5)
#define RGBf36 (2*x - 1)

#define MAX_COMPONENT_VALUE 255
//void writePPMbinaryImage(const char * filename, const cufftComplex * vector)
void writePPMbinaryImage(const char * filename, const FLOAT * vector)
{
	FILE *f = NULL;
	unsigned int i = 0, j = 0;
	uint8_t RGB[3] = {0,0,0}; /* use 3 first bytes for BGR */ 
	f = fopen(filename, "w");
	assert(f);
	fprintf(f, "P6\n"); /* Portable colormap, binary */
	fprintf(f, "%d %d\n", LY, LX); /* Image size */
	fprintf(f, "%d\n", MAX_COMPONENT_VALUE); /* Max component value */
	for (i=0; i<LX; i++){
		for (j=0; j<LY; j++){
			/* ColorMaps
			 * http://mainline.brynmawr.edu/Courses/cs120/spring2008/Labs/ColorMaps/colorMap.py
			 */
			FLOAT x = vector[i*LY+j]; //HOT; /* [0,1] */
//			RGB[0] = (uint8_t) 256*RGBf07;  //21
//			RGB[1] = (uint8_t) 256*RGBf05;  //22
//			RGB[2] = (uint8_t) 256*RGBf15;  //23
			RGB[0] = (uint8_t) MIN(255, MAX(0, 256*RGBf07));  //21
			RGB[1] = (uint8_t) MIN(255, MAX(0, 256*RGBf05));  //22
			RGB[2] = (uint8_t) MIN(255, MAX(0, 256*RGBf15));  //23
			//assert(0<=RGB[0] && RGB[0]<256);
			//assert(0<=RGB[1] && RGB[1]<256);
			//assert(0<=RGB[2] && RGB[2]<256);
			fwrite((void *)RGB, 3, 1, f);
		}
	}
	fclose(f); f = NULL;
	return;
}

//////////////////////////////////////////////////////////////////////
class phi4_model
{
	private:
	// cuFFT plan
	cufftHandle Sr2c; 

	public:

	// real arrays to hold the scalar field phi and the force
	FLOAT *h_phi_r, *h_force_r;
	FLOAT *d_phi_r, *d_force_r;

	// complex arrays to hold the transformed scalar field F[phi]
	cufftComplex *d_phi; 

	// FLOAT array for the Fourier modes weights (the q's)
	FLOAT *d_qmodule; 

	// FLOAT array for the quenched disorder
	FLOAT *d_disorder;
	
	//TODO:Arrays for the physical quantities
	
	// intializing the class
	phi4_model(){

		h_phi_r = (FLOAT *)malloc(sizeof(FLOAT)*LX*LY);
		h_force_r = (FLOAT *)malloc(sizeof(FLOAT)*LX*LY);
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi_r, sizeof(FLOAT)*LX*LY));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_force_r, sizeof(FLOAT)*LX*LY));

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi, sizeof(cufftComplex)*LX*(LY/2+1)));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_qmodule, sizeof(FLOAT)*LX*(LY/2+1))); 

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_disorder, sizeof(FLOAT)*LX*LY)); 

		/* Fill everything with 0s */

		for (unsigned int k=0; k<LX*LY; k++){
			h_phi_r[k] = (FLOAT) 0;
			h_force_r[k] = (FLOAT) 0;
		}

		CUDA_SAFE_CALL(cudaMemset(d_phi_r, 0, LX*LY*sizeof(FLOAT)));
		CUDA_SAFE_CALL(cudaMemset(d_force_r, 0, LX*LY*sizeof(FLOAT))); 

		CUDA_SAFE_CALL(cudaMemset(d_phi, 0, LX*(LY/2+1)*sizeof(cufftComplex)));
		CUDA_SAFE_CALL(cudaMemset(d_qmodule, 0, LX*(LY/2+1)*sizeof(FLOAT)));

		CUDA_SAFE_CALL(cudaMemset(d_disorder, 0, LX*LY*sizeof(FLOAT))); 

		// cuFFT plans R2C
		CUFFT_SAFE_CALL(cufftPlan2d(&Sr2c,LX,LY,CUFFT_R2C));
	//	cufftSetCompatibilityMode(Sr2c, CUFFT_COMPATIBILITY_NATIVE);

	}


////////// Initializing Functions //////////

	void InitParticular(const int nx, const int ny, const int shape, const FLOAT x_offset, const FLOAT y_offset, const FLOAT width, const FLOAT angle){
		for(int i=0;i<nx;i++){
			for(int j=0;j<ny;j++){
				int k=i*ny+j;

				// flat interfase
				if (shape==0) h_phi_r[k] = (ny*(0.5+y_offset) < j)?(1.0):(-1.0);
	
				// band
				if (shape==1) h_phi_r[k] = (ny*(0.5-width*0.5+y_offset) < j && j < ny*(0.5+width*0.5+y_offset))?(1.0):(-1.0);
				//h_phi_r[k]= (i+j<ny*0.5 || (nx-i)+(ny-j)<ny*0.5)?(1.0):(-1.0);

				// circle
 				if (shape==2){
				const FLOAT a = i-nx*(0.5+x_offset);
				const FLOAT b = j-ny*(0.5+y_offset);
				const FLOAT c = ny*width*0.5;
				h_phi_r[k] = ( a*a + b*b < c*c )?(1.0):(-1.0);
				}

				// square_with_hole
				// TODO: generalize this case if desired
 				if (shape==3){
				h_phi_r[k]= (nx*0.35 < i && i < nx*0.65 && ny*0.35 < j && j < ny*0.65)?(1.0):(0.0);
				h_phi_r[k]= (nx*0.45 < i && i < nx*0.55 && ny*0.45 < j && j < ny*0.55)?(0.0):(h_phi_r[k]);
				}

				// parallel stripes
				// TODO: Think about preserving periodic boundary conditions
				if (shape==4) h_phi_r[k]= (cosf((sinf(angle)*j+cosf(angle)*i)*2*M_PI/(width*ny)) > 0.f)?(1.0):(-1.0);
			}
		}
 		CpyHostToDevice();
	};

	void InitRandom(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_InitRandom<<<dimGrid, dimBlock>>>(d_phi_r,LX,LY);
	};

	void InitFlat(const FLOAT value){
		CUDA_SAFE_CALL(cudaMemset(d_phi_r, value, LX*LY*sizeof(FLOAT)));
	};

	void SetQxQy(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_SetQxQy<<<dimGrid, dimBlock>>>(d_qmodule,LX,LY/2+1);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, qmoduleTex, d_qmodule, LX*(LY/2+1)*sizeof(FLOAT)));
		
	};

	void SetDisorder(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_SetDisorder<<<dimGrid, dimBlock>>>(d_disorder,LX,LY);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, disorderTex, d_disorder, LX*LY*sizeof(FLOAT)));
	};


////////// Copy Functions //////////

	/* transfer from CPU to GPU memory */
	void CpyHostToDevice(){
		CUDA_SAFE_CALL(cudaMemcpy(d_phi_r,h_phi_r, sizeof(FLOAT)*LX*LY, cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpy(d_force_r,h_force_r, sizeof(FLOAT)*LX*LY, cudaMemcpyHostToDevice));
	}

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		CUDA_SAFE_CALL(cudaMemcpy(h_phi_r, d_phi_r, sizeof(FLOAT)*LX*LY, cudaMemcpyDeviceToHost));
	}

////////// Fourier Transform Functions //////////

	void TransformToFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecR2C(Sr2c, d_phi_r, d_phi));
	};

////////// Evolution Functions //////////

	void CalculateRealForce(const FLOAT uniform_field, const FLOAT offset, const FLOAT disordershiftx){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		CUDAkernel_CalculateRealForce<<<dimGrid, dimBlock>>>(d_disorder,d_phi_r,d_force_r,LX,LY,uniform_field,offset,disordershiftx);
	}

	void EulerStep(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		euler_step_rs<<<dimGrid, dimBlock>>>(d_phi_r, d_force_r, LX, LY);
	};

/*	void EulerStepSemiimplicit(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		euler_semiimplicit_step<<<dimGrid, dimBlock>>>(d_phi, d_force, d_qmodule, LX, (LY/2+1));
	};
*/

////////// Calculation Functions /////


	int CountInterfaces(unsigned int time){

		unsigned int N=LX*LY;

		thrust::device_ptr<FLOAT> d_phi_r_ptr(d_phi_r);

		thrust::device_vector<FLOAT>::iterator it_begin = d_phi_r_ptr;
		thrust::device_vector<FLOAT>::iterator it_shift_begin = d_phi_r_ptr+1;

		thrust::device_vector<FLOAT>::iterator it_end = d_phi_r_ptr+N-1;
		thrust::device_vector<FLOAT>::iterator it_shift_end = d_phi_r_ptr+N;

		thrust::counting_iterator<unsigned int> index_begin(0);
		thrust::counting_iterator<unsigned int> index_end(N-1);

		transf_iter_is_wall it0b(
			make_zip_iterator(make_tuple(it_begin, it_shift_begin, index_begin)), 
			is_wall()
		);

		transf_iter_is_wall it1b(
			make_zip_iterator(make_tuple(it_end, it_shift_end, index_end)),
			is_wall()
		);

		int nparedes =  N-thrust::count(it0b, it1b,-1);

		return(nparedes);
	}

////////// Print Functions //////////

	void PrintData(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << setprecision (4) << h_phi_r[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintPicture(char *picturename, int x){
	char cmd[200];	char cmd2[200];
	writePPMbinaryImage(picturename, h_phi_r);
	sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
	system(cmd);
	sprintf(cmd2, "rm frame%d.ppm", 100000000+x);
	system(cmd2);
	};
	

/////////////// Cleaning Issues ////////////////

	~phi4_model(){
		cufftDestroy(Sr2c);
		cudaFree(d_phi_r);
		cudaFree(d_force_r);
		free(h_phi_r);
		free(h_force_r);
		cudaFree(d_phi);
		CUDA_SAFE_CALL(cudaUnbindTexture(disorderTex));
		CUDA_SAFE_CALL(cudaUnbindTexture(qmoduleTex));
		cudaFree(d_qmodule);
		cudaFree(d_disorder);
	};
};

#endif /* PHI4_LIBRARY_CUH */
